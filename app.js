//Class Portafolio and profit method
const { Portafolio } = require('./portafolio');
//JSON file with example data from googlefinance
const { loadData } = require('./loaddata');


let WBuffett = new Portafolio("Portafolio's WBuffett", loadData());

let start_date = '08/04/2020';
let end_date = '11/11/2020';
console.log("---------------------");
console.log(`This code calculates the profit between the range: (${start_date} and ${end_date})`);

let profit = WBuffett.profit(start_date, end_date);
console.log("---------------------");
console.log("---------------------");
console.log(profit);