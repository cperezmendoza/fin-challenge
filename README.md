***
# Description

The Class "Portfolio" contains a "profit" method that receives a range of dates and calculates the annualized return.
```
let start_date = '08/04/2020';
let end_date = '11/11/2020';
let profit = WBuffett.profit(start_date, end_date);
```
***
# Results

```
This code calculates the profit between the range: (08/04/2020 and 11/11/2020)
```

```
Stock: Vodafone
Prices found in range: 218 price(s)
First price in range: GBP 1.1084
Last price in range: GBP 1.1668
Gain: 0.058
Days invested: 216
Total return: 5.27%
Annualized return: 9.064%
```

```
Stock: Telefonica
Prices found in range: 167 price(s)
First price in range: EUR 4.24
Last price in range: EUR 3.39
Gain: -0.850
Days invested: 165
Total return: -20.05%
Annualized return: -39.038%
```

```
Stock: Apple
Prices found in range: 115 price(s)
First price in range: USD 98.36
Last price in range: USD 115.97
Gain: 17.610
Days invested: 113
Total return: 17.90%
Annualized return: 70.231%
```

```
Stock: Disney
Prices found in range: 59 price(s)
First price in range: USD 131.25
Last price in range: USD 142.11
Gain: 10.860
Days invested: 58
Total return: 8.27%
Annualized return: 64.919%
```

```
Stock: 3M
Prices found in range: 21 price(s)
First price in range: USD 170.71
Last price in range: USD 169.14
Gain: -1.570
Days invested: 20
Total return: -0.92%
Annualized return: -15.517%
```
***

# Return
```
[
  { nombre: 'Vodafone', annualized_return: '9.064%' },
  { nombre: 'Telefonica', annualized_return: '-39.038%' },
  { nombre: 'Apple', annualized_return: '70.231%' },
  { nombre: 'Disney', annualized_return: '64.919%' },
  { nombre: '3M', annualized_return: '-15.517%' }
]

```

