//Data from GOOGLEFINANCE
const loadData = () => {
    let data = {};
    try {
        data.stocks = require('./bd/stock.json');
    } catch (error) {
        data.prices = [];
        data.stocks = [];
    }
    return data;
}
module.exports = {
    loadData
}