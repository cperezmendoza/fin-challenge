class Portafolio {
    constructor(name, data) {
        this.name = name;
        this.stocks = data.stocks
        this.prices = data.prices
    };
    /**
     * Loop through json data and calculate profit
     * @param {string} start_date Initial date to get data from json
     * @param {string} end_date End date to get data from json
     * @return {number} The profit of the Portfolio between those dates
     */
    profit = (start_date, end_date) => {
        let stocks_and_profit = []
            //Loop through json data that contains prices from diferent dates to today
        for (let stock of this.stocks) {
            let prices_in_range = []
            for (let price of stock.prices) {
                let date_price = new Date(price.date);
                //if the price of the stock is between the range, that price is added to a new array
                if (stringToDate(start_date, false) <= date_price && stringToDate(end_date, false) >= date_price) {
                    prices_in_range.push(price)
                }
            }

            let StockCurrentPrice = currentElement(stock.prices);
            console.log("---------------------");
            console.log(`Stock: ${stock.name}`);
            //console.log(`Current Price (today): ${stock.currency} ${StockCurrentPrice.price} `);
            //console.log(`Prices in data base: ${stock.prices.length} recorded dates`);

            if (prices_in_range.length) {
                let InitialPrice = initialElement(prices_in_range);
                let CurrentPrice = currentElement(prices_in_range);
                let float_initialprice = parseFloat(InitialPrice.price);
                let float_currentprice = parseFloat(CurrentPrice.price);

                console.log(`Prices found in range: ${prices_in_range.length} price(s)`);

                //First and last price in range
                console.log(`First price in range: ${stock.currency} ${InitialPrice.price}  `);
                console.log(`Last price in range: ${stock.currency} ${CurrentPrice.price} `);

                //Gain
                let gain = (float_currentprice - float_initialprice);
                console.log(`Gain: ${gain.toFixed(3)}`);

                //Days invested
                let diff_in_time = stringToDate(CurrentPrice.date).getTime() - stringToDate(InitialPrice.date).getTime();
                let diff_in_days = Math.floor(diff_in_time / (1000 * 3600 * 24));
                console.log(`Days invested: ${diff_in_days}`);

                //Total return
                let total_returned = parseFloat((float_currentprice - float_initialprice) / float_initialprice) * 100;
                console.log(`Total return: ${total_returned.toFixed(2) + "%"}`);

                //Annualized return
                let annualized_return = (((float_initialprice + gain) / float_initialprice) ** (365 / diff_in_days) - 1) * 100;
                console.log(`Annualized return: ${annualized_return.toFixed(3)+'%'}`);
                stocks_and_profit.push({
                    'nombre': stock.name,
                    'annualized_return': annualized_return.toFixed(3) + '%'
                })

            } else {
                console.log(`Prices found in range: 0 price(s)`);
            }
        }

        return stocks_and_profit;
    }
}
/**
 * Converts a string to date
 * @param {string} string_date The string that contains the date dd/mm/yyyy
 * @param {boolean} yyyymmdd This bolean identifies if the string_date comes as dd/mm/yyyy or yyyy/mm/dd
 * @return {Date} The Date object
 */
let stringToDate = (string_date, yyyymmdd = true) => {
    var parts = string_date.split('/');
    if (yyyymmdd) {
        return new Date(parts[0], parts[1] - 1, parts[2]);
    } else {
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
}

/**
 * Gets the last element from an array
 * @param {Array} element
 * @return {object} last element
 */
let currentElement = (element) => {
    return element[element.length - 1];
}

/**
 * Gets the first element from an array
 * @param {Array} element
 * @return {object} first element
 */
let initialElement = (element) => {
    return element[0];
}

module.exports = {
    Portafolio
}